package com.dh.fullstack.users.service.service;

import com.dh.fullstack.users.service.exception.UserNotFoundException;
import com.dh.fullstack.users.service.framework.context.ServiceTransactional;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.AccountType;
import com.dh.fullstack.users.service.model.domain.User;
import com.dh.fullstack.users.service.model.repositories.AccountRepository;
import com.dh.fullstack.users.service.model.repositories.CompanyRepository;
import com.dh.fullstack.users.service.model.repositories.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
@ServiceTransactional
public class RootUserReadByEmailService {

    private static Logger LOG = LoggerFactory.getLogger(RootUserReadByEmailService.class);

    private String email;

    private User user;

    private Account account;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public void execute() {
        LOG.info("=== Find user by email from auth");
        account = findAccountByEmail(email);
        user = findUserByAccount(account);
    }

    private Account findAccountByEmail(String email) {
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("Unable locate an account to email = " + email));
    }

    private User findUserByAccount(Account account) {
        if (AccountType.COMPANY.equals(account.getAccountType())) {
            return companyRepository.findByAccount(account)
                    .orElseThrow(() -> new UserNotFoundException("Unable locate an user to email = " + email));
        } else {
            return employeeRepository.findByAccount(account)
                    .orElseThrow(() -> new UserNotFoundException("Unable locate an user to email = " + email));
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Account getAccount() {
        return account;
    }

    public User getUser() {
        return user;
    }
}
