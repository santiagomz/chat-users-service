package com.dh.fullstack.users.service.model.impl;

import com.dh.fullstack.users.api.model.AccountState;
import com.dh.fullstack.users.api.model.system.RootUser;

/**
 * @author Santiago Mamani
 */
public class RootUserImpl implements RootUser {

    private Long userId;

    private Long accountId;

    private String email;

    private String password;

    private AccountState accountStatus;

    @Override
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public AccountState getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountState accountStatus) {
        this.accountStatus = accountStatus;
    }
}
