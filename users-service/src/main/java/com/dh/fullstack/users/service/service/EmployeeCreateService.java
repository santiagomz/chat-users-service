package com.dh.fullstack.users.service.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.fullstack.users.api.model.AccountState;
import com.dh.fullstack.users.service.client.contact.service.SystemContactService;
import com.dh.fullstack.users.service.framework.context.ServiceTransactional;
import com.dh.fullstack.users.service.input.EmployeeCreateInput;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.AccountType;
import com.dh.fullstack.users.service.model.domain.Employee;
import com.dh.fullstack.users.api.model.UserType;
import com.dh.fullstack.users.service.model.repositories.AccountRepository;
import com.dh.fullstack.users.service.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Date;

/**
 * @author Santiago Mamani
 */
@ServiceTransactional
public class EmployeeCreateService {

    private EmployeeCreateInput input;

    @Autowired
    private SystemContactService systemContactService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccountRepository accountRepository;

    private Employee employee;

    public void execute(){
        Account account = composeAccountInstance();
        account = accountRepository.save(account);

        String passwordEncoded = passwordEncode(input);

        Employee employeeInstance = composeEmployeeInstance(account, passwordEncoded);
        employee = employeeRepository.save(employeeInstance);

        createContact(account, employee);
    }

    private String passwordEncode(EmployeeCreateInput input) {
        return new BCryptPasswordEncoder().encode(input.getPassword());
    }

    private void createContact(Account account, Employee employee) {
        SystemContactCreateInput input = new SystemContactCreateInput();
        input.setAccountId(account.getId());
        input.setUserId(employee.getId());
        input.setName(employee.getLastName() + " " + employee.getFirstName());
        input.setEmail(employee.getEmail());

        systemContactService.createContact(input);
    }

    private Account composeAccountInstance() {
        Account instance = new Account();
        instance.setState(AccountState.DEACTIVATED);
        instance.setAccountType(AccountType.EMPLOYEE);
        instance.setEmail(input.getEmail());

        return instance;
    }

    private Employee composeEmployeeInstance(Account account, String passwordEncoded) {
        Employee instance = new Employee();
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());
        instance.setEmail(input.getEmail());
        instance.setPassword(passwordEncoded);
        instance.setActive(Boolean.TRUE);
        instance.setUserType(UserType.EMPLOYEE);
        instance.setCreatedDate(new Date());
        instance.setAccount(account);

        return instance;
    }

    public void setInput(EmployeeCreateInput input) {
        this.input = input;
    }

    public Employee getEmployee() {
        return employee;
    }
}
