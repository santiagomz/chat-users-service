package com.dh.fullstack.users.service.model.domain;

/**
 * @author Santiago Mamani
 */
public enum AccountType {
    COMPANY,
    EMPLOYEE
}
