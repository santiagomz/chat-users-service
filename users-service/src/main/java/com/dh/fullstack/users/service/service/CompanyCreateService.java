package com.dh.fullstack.users.service.service;

import com.dh.fullstack.users.api.model.UserType;
import com.dh.fullstack.users.service.framework.context.ServiceTransactional;
import com.dh.fullstack.users.service.input.CompanyCreateInput;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.Company;
import com.dh.fullstack.users.service.model.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Santiago Mamani
 */
@ServiceTransactional
public class CompanyCreateService {

    private CompanyCreateInput input;

    @Autowired
    private CompanyAccountCreateService companyAccountCreateService;

    @Autowired
    private CompanyRepository companyRepository;

    private Company company;

    public void execute(){
        Account account= createAccount();
        // generateError(); Descomentar para probar la transaccion
        String passwordEncoded = passwordEncode(input);
        company = companyRepository.save(composeCompanyInstance(account, passwordEncoded));
    }

    private Account createAccount() {
        companyAccountCreateService.setInput(input);
        companyAccountCreateService.execute();

        return companyAccountCreateService.getAccount();
    }

    private void generateError() {
        throw  new RuntimeException("Error in account table.");
    }

    private String passwordEncode(CompanyCreateInput input) {
        return new BCryptPasswordEncoder().encode(input.getPassword());
    }

    private Company composeCompanyInstance(Account account, String passwordEncoded) {
        Company instance = new Company();
        instance.setName(input.getName());
        instance.setEmail(input.getEmail());
        instance.setActive(Boolean.TRUE);
        instance.setUserType(UserType.COMPANY);
        instance.setPassword(passwordEncoded);
        instance.setAccount(account);

        return instance;
    }

    public void setInput(CompanyCreateInput input) {
        this.input = input;
    }

    public Company getCompany() {
        return company;
    }
}
