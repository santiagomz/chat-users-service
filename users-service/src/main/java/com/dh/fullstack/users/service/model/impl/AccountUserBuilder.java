package com.dh.fullstack.users.service.model.impl;

import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.User;

/**
 * @author Santiago Mamani
 */
public final class AccountUserBuilder {

    private AccountUserImpl instance;

    public static AccountUserBuilder getInstance(User user, Account account) {
        return (new AccountUserBuilder()).setUser(user).setAccount(account);
    }

    private AccountUserBuilder() {
        instance = new AccountUserImpl();
    }

    private AccountUserBuilder setUser(User user) {
        instance.setUserId(user.getId());
        instance.setUserType(user.getUserType());

        return this;
    }

    private AccountUserBuilder setAccount(Account account) {
        instance.setAccountId(account.getId());

        return this;
    }

    public AccountUserImpl build() {
        return instance;
    }
}
