package com.dh.fullstack.users.service.service;

import com.dh.fullstack.users.api.model.AccountState;
import com.dh.fullstack.users.service.config.UsersMyProperties;
import com.dh.fullstack.users.service.config.UsersProperties;
import com.dh.fullstack.users.service.framework.context.ServiceTransactional;
import com.dh.fullstack.users.service.input.AccountInput;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.AccountType;
import com.dh.fullstack.users.service.model.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
@ServiceTransactional
public class AccountCreateService {

    private static Logger LOG = LoggerFactory.getLogger(AccountCreateService.class);

    private AccountInput input;

    @Autowired
    private UsersProperties usersProperties;

    @Autowired
    private UsersMyProperties usersMyProperties;

    @Autowired
    private AccountRepository accountRepository;

    public Account save() {
        LOG.info("countUser: " + usersProperties.getCountUser());
        LOG.info("nameAsus: " + usersProperties.getNameAsus());
        LOG.info("apiVersion: " + usersMyProperties.getApiVersion());

        return accountRepository.save(composeAccountInstance());
    }

    private Account composeAccountInstance() {
        AccountState state=AccountState.DEACTIVATED;
        Boolean isPermit =  isPermit();
        if (isPermit){
            state = AccountState.ACTIVATED;
        }

        Account instance = new Account();
        instance.setState(state);
        instance.setEmail(input.getEmail());
        instance.setAccountType(AccountType.EMPLOYEE);

        return instance;
    }

    private Boolean isPermit(){
       return input.getAge()>=usersProperties.getPermitAge();
    }

    public void setInput(AccountInput input) {
        this.input = input;
    }
}
