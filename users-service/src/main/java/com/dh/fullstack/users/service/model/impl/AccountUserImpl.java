package com.dh.fullstack.users.service.model.impl;

import com.dh.fullstack.users.api.model.UserType;
import com.dh.fullstack.users.api.model.system.AccountUser;

/**
 * @author Santiago Mamani
 */
public class AccountUserImpl implements AccountUser {

    private Long userId;

    private Long accountId;

    private UserType userType;

    @Override
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
