package com.dh.fullstack.users.service.model.repositories;

import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Santiago Mamani
 */
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    @Query("select item from Employee item where item.account = :account")
    Optional<Employee> findByAccount(@Param("account") Account account);

    @Query("select item from Employee item where item.id = :userId and item.account = :account")
    Optional<Employee> findByIdAndAccount(@Param("userId") Long userId, @Param("account") Account account);
}
