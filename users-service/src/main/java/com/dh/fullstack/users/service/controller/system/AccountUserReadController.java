package com.dh.fullstack.users.service.controller.system;

import com.dh.fullstack.users.api.model.system.AccountUser;
import com.dh.fullstack.users.service.controller.Constants;
import com.dh.fullstack.users.service.model.impl.AccountUserBuilder;
import com.dh.fullstack.users.service.service.AccountUserReadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = Constants.AccountsTag.NAME,
        description = Constants.AccountsTag.DESCRIPTION
)
@RequestMapping(value = Constants.BasePath.SYSTEM_ACCOUNTS)
@RestController
@RequestScope
public class AccountUserReadController {

    @Autowired
    private AccountUserReadService accountUserReadService;

    @ApiOperation(
            value = "Get a single account user."
    )
    @RequestMapping(
            value = "/{accountId}/users/{userId}",
            method = RequestMethod.GET
    )
    public AccountUser readAccountUser(@PathVariable("accountId") Long accountId, @PathVariable("userId") Long userId) {
        accountUserReadService.setAccountId(accountId);
        accountUserReadService.setUserId(userId);
        accountUserReadService.execute();

        return AccountUserBuilder.getInstance(accountUserReadService.getUser(),
                accountUserReadService.getAccount())
                .build();
    }
}
