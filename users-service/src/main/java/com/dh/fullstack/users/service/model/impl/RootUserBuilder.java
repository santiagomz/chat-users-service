package com.dh.fullstack.users.service.model.impl;

import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.User;

/**
 * @author Santiago Mamani
 */
public final class RootUserBuilder {

    private RootUserImpl instance;

    public static RootUserBuilder getInstance(User user, Account account) {
        return (new RootUserBuilder()).setUser(user).setAccount(account);
    }

    private RootUserBuilder() {
        instance = new RootUserImpl();
    }

    private RootUserBuilder setUser(User user) {
        instance.setUserId(user.getId());
        instance.setEmail(user.getEmail());
        instance.setPassword(user.getPassword());

        return this;
    }

    private RootUserBuilder setAccount(Account account) {
        instance.setAccountId(account.getId());
        instance.setAccountStatus(account.getState());

        return this;
    }

    public RootUserImpl build() {
        return instance;
    }
}
