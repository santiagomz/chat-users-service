package com.dh.fullstack.users.service.controller.system;

import com.dh.fullstack.users.api.model.system.RootUser;
import com.dh.fullstack.users.service.controller.Constants;
import com.dh.fullstack.users.service.model.impl.RootUserBuilder;
import com.dh.fullstack.users.service.service.RootUserReadByEmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = Constants.AccountsTag.NAME,
        description = Constants.AccountsTag.DESCRIPTION
)
@RestController
@RequestMapping(Constants.BasePath.SYSTEM_ACCOUNTS)
@RequestScope
public class UserReadByEmailController {

    @Autowired
    private RootUserReadByEmailService rootUserReadByEmailService;

    @ApiOperation(
            value = "Get user account by email."
    )
    @RequestMapping(
            method = RequestMethod.GET
    )
    public RootUser findAccountUser(@RequestParam(value = "email") String email) {
        rootUserReadByEmailService.setEmail(email);
        rootUserReadByEmailService.execute();

        return RootUserBuilder.getInstance(rootUserReadByEmailService.getUser(),
                rootUserReadByEmailService.getAccount()).build();
    }
}
