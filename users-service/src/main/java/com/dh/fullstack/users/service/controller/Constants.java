package com.dh.fullstack.users.service.controller;

/**
 * @author Santiago Mamani
 */
public final class Constants {

    private Constants() {
    }

    public static class AccountsTag {
        public static final String NAME = "accounts-controller";

        public static final String DESCRIPTION = "Available operations over accounts";
    }

    public static class CompanyTag {
        public static final String NAME = "company-controller";

        public static final String DESCRIPTION = "Available operations over companies";
    }

    public static class EmployeeTag {
        public static final String NAME = "employee-controller";

        public static final String DESCRIPTION = "Available operations over emplyees";
    }


    public static class BasePath {
        public static final String PUBLIC = "/public";

        public static final String SECURE = "/secure";

        public static final String SYSTEM = "/system";

        public static final String PUBLIC_COMPANIES = PUBLIC + "/companies";

        public static final String PUBLIC_EMPLOYEES = PUBLIC + "/employees";

        public static final String SYSTEM_ACCOUNTS = SYSTEM + "/accounts";

        public static final String SECURE_ACCOUNTS = SECURE + "/accounts";
    }
}
