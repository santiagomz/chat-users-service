package com.dh.fullstack.users.service.service;

import com.dh.fullstack.users.service.exception.UserNotFoundException;
import com.dh.fullstack.users.service.framework.context.ServiceTransactional;
import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.AccountType;
import com.dh.fullstack.users.service.model.domain.User;
import com.dh.fullstack.users.service.model.repositories.AccountRepository;
import com.dh.fullstack.users.service.model.repositories.CompanyRepository;
import com.dh.fullstack.users.service.model.repositories.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Santiago Mamani
 */
@ServiceTransactional
public class AccountUserReadService {

    private static Logger LOG = LoggerFactory.getLogger(AccountUserReadService.class);

    private Long accountId;

    private Long userId;

    private Account account;

    private User user;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public void execute() {
        LOG.info("=== Find user by accountId and userId from gateway");
        account = findAccount(accountId);
        user = findUser(userId, account);
    }

    private Account findAccount(Long accountId) {
        return accountRepository.findById(accountId)
                .orElseThrow(() -> new UserNotFoundException("Unable locate an account for accountId = " + accountId));
    }

    private User findUser(Long userId, Account account) {
        if (AccountType.COMPANY.equals(account.getAccountType())) {
            return companyRepository.findByIdAndAccount(userId, account)
                    .orElseThrow(() -> new UserNotFoundException("Unable locate an user to userId = " + userId));
        } else {
            return employeeRepository.findByIdAndAccount(userId, account)
                    .orElseThrow(() -> new UserNotFoundException("Unable locate an user to userId = " + userId));
        }
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Account getAccount() {
        return account;
    }

    public User getUser() {
        return user;
    }
}
