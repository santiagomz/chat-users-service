package com.dh.fullstack.users.service.model.repositories;

import com.dh.fullstack.users.service.model.domain.Account;
import com.dh.fullstack.users.service.model.domain.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * @author Santiago Mamani
 */
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query("select item from Company item where item.account = :account")
    Optional<Company> findByAccount(@Param("account") Account account);

    @Query("select item from Company item where item.id = :userId and item.account = :account")
    Optional<Company> findByIdAndAccount(@Param("userId") Long userId, @Param("account") Account account);
}
