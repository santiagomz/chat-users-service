package com.dh.fullstack.users.api.model;

/**
 * @author Santiago Mamani
 */
public enum UserType {
    COMPANY,
    EMPLOYEE
}
