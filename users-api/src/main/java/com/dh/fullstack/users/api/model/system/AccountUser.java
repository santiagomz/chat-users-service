package com.dh.fullstack.users.api.model.system;

import com.dh.fullstack.users.api.model.UserType;

import java.io.Serializable;

/**
 * @author Santiago Mamani
 */
public interface AccountUser extends Serializable {

    Long getAccountId();

    Long getUserId();

    UserType getUserType();
}
