package com.dh.fullstack.users.api.model.system;

import com.dh.fullstack.users.api.model.AccountState;

import java.io.Serializable;

/**
 * @author Santiago Mamani
 */
public interface RootUser extends Serializable {

    Long getUserId();

    Long getAccountId();

    String getEmail();

    String getPassword();

    AccountState getAccountStatus();
}
